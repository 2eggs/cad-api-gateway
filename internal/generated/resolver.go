package generated

// THIS CODE IS A STARTING POINT ONLY. IT WILL NOT BE UPDATED WITH SCHEMA CHANGES.

import (
	"context"

	"gitlab.com/mayunmeiyouming/cad-micro/rpc/cad"
)

type Resolver struct{}

func (r *mutationResolver) Healthcheck(ctx context.Context) (string, error) {
	panic("not implemented")
}

func (r *queryResolver) Healthcheck(ctx context.Context) (string, error) {
	panic("not implemented")
}

func (r *queryResolver) QueryCompanyListByCompanyName(ctx context.Context, companyName string) (*cad.CompanyInfoResponse, error) {
	panic("not implemented")
}

func (r *queryResolver) QueryCompanyOverviewByCompanyID(ctx context.Context, companyID string) (*cad.CompanyOverviewResponse, error) {
	panic("not implemented")
}

func (r *queryResolver) QueryFinancingListByCompanyID(ctx context.Context, first *int64, after *int64, last *int64, before *int64, companyID string) (*cad.FinancingDealConnection, error) {
	panic("not implemented")
}

func (r *queryResolver) QueryInvestmentListByCompanyID(ctx context.Context, first *int64, after *int64, last *int64, before *int64, companyID string) (*cad.InvestmentTransactionConnection, error) {
	panic("not implemented")
}

func (r *queryResolver) QueryExternalAcquisitionListByCompanyID(ctx context.Context, first *int64, after *int64, last *int64, before *int64, companyID string) (*cad.ExternalAcquisitionConnection, error) {
	panic("not implemented")
}

func (r *queryResolver) QueryMergerInformationByCompanyID(ctx context.Context, companyID string) (*cad.MergerInfoResponse, error) {
	panic("not implemented")
}

func (r *queryResolver) QueryFinancingOverviewByFinancingIndex(ctx context.Context, companyID string, fundingRoundType string, fundedAt string, raisedAmountUsd int64) (*cad.FinancingOverviewResponse, error) {
	panic("not implemented")
}

func (r *queryResolver) QueryFinancingInvestorListByFinancingIndex(ctx context.Context, first *int64, after *int64, last *int64, before *int64, companyID string, fundingRoundType string, fundedAt string, raisedAmountUsd int64) (*cad.InvestorListConnection, error) {
	panic("not implemented")
}

// Mutation returns MutationResolver implementation.
func (r *Resolver) Mutation() MutationResolver { return &mutationResolver{r} }

// Query returns QueryResolver implementation.
func (r *Resolver) Query() QueryResolver { return &queryResolver{r} }

type mutationResolver struct{ *Resolver }
type queryResolver struct{ *Resolver }
