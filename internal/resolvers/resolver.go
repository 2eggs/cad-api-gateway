package resolvers

// THIS CODE IS A STARTING POINT ONLY. IT WILL NOT BE UPDATED WITH SCHEMA CHANGES.

import (
	"context"

	"gitlab.com/mayunmeiyouming/cad-micro/rpc/cad"

	"log"

	wrapperspb "github.com/golang/protobuf/ptypes/wrappers"
)

// Resolver ...
type Resolver struct {
	Client cad.CadTwirp
}

func (r *mutationResolver) Healthcheck(ctx context.Context) (string, error) {
	panic("not implemented")
}

func (r *queryResolver) Healthcheck(ctx context.Context) (string, error) {
	panic("not implemented")
}

func (r *queryResolver) QueryCompanyListByCompanyName(ctx context.Context, companyName string) (*cad.CompanyInfoResponse, error) {
	companyInfoResponse, err := r.Client.QueryCompanyListByCompanyName(
		context.Background(),
		&cad.QueryCompanyListByCompanyNameRequest{
			CompanyName: companyName,
		},
	)

	if err != nil {
		return nil, err
	}

	return companyInfoResponse, nil
}

func (r *queryResolver) QueryCompanyOverviewByCompanyID(ctx context.Context, companyID string) (*cad.CompanyOverviewResponse, error) {
	companyInfo, err := r.Client.QueryCompanyOverviewByCompanyID(
		context.Background(),
		&cad.QueryCompanyOverviewByCompanyIDRequest{
			CompanyId: companyID,
		},
	)

	if err != nil {
		return nil, err
	}

	return companyInfo, nil
}

func (r *queryResolver) QueryFinancingListByCompanyID(
	ctx context.Context,
	first *int64,
	after *int64,
	last *int64,
	before *int64,
	companyID string) (*cad.FinancingDealConnection, error) {

	log.Println("QueryFinancingListByCompanyID")
	log.Println("First: ", first)
	log.Println("After: ", after)

	var newFirst *wrapperspb.Int64Value 
	var newAfter *wrapperspb.Int64Value
	var newLast *wrapperspb.Int64Value
	var newBefore *wrapperspb.Int64Value
	if first != nil {
		newFirst = &wrapperspb.Int64Value {
			Value: *first,
		}
	}
	if after != nil {
		newAfter = &wrapperspb.Int64Value {
			Value: *after,
		}
	}
	if last != nil {
		newLast = &wrapperspb.Int64Value {
			Value: *last,
		}
	}
	if before != nil {
		newBefore = &wrapperspb.Int64Value {
			Value: *before,
		}
	}
	
	
	companiesRoundsConn, err := r.Client.QueryFinancingListByCompanyID(
		context.Background(),
		&cad.QueryFinancingListByCompanyIDRequest{
			First:     newFirst,
			After:     newAfter,
			Last:      newLast,
			Before:    newBefore,
			CompanyId: companyID,
		},
	)
	if err != nil {
		log.Println(err)
		return nil, err
	}

	return companiesRoundsConn, nil
}

func (r *queryResolver) QueryInvestmentListByCompanyID(
	ctx context.Context,
	first *int64,
	after *int64,
	last *int64,
	before *int64,
	companyID string) (*cad.InvestmentTransactionConnection, error) {

	log.Println("QueryInvestmentListByCompanyID")
	log.Println("First: ", *first)
	log.Println("After: ", *after)
	log.Println("CompanyID: ", companyID)

	var newFirst *wrapperspb.Int64Value 
	var newAfter *wrapperspb.Int64Value
	var newLast *wrapperspb.Int64Value
	var newBefore *wrapperspb.Int64Value
	if first != nil {
		newFirst = &wrapperspb.Int64Value {
			Value: *first,
		}
	}
	if after != nil {
		newAfter = &wrapperspb.Int64Value {
			Value: *after,
		}
	}
	if last != nil {
		newLast = &wrapperspb.Int64Value {
			Value: *last,
		}
	}
	if before != nil {
		newBefore = &wrapperspb.Int64Value {
			Value: *before,
		}
	}

	investmentListConn, err := r.Client.QueryInvestmentListByCompanyID(
		context.Background(),
		&cad.QueryInvestmentListByCompanyIDRequest{
			First:     newFirst,
			After:     newAfter,
			Last:      newLast,
			Before:    newBefore,
			CompanyId: companyID,
		},
	)

	if err != nil {
		log.Println(err)
		return investmentListConn, err
	}

	return investmentListConn, nil
}

func (r *queryResolver) QueryExternalAcquisitionListByCompanyID(ctx context.Context, 
	first *int64,
	after *int64,
	last *int64,
	before *int64,
	companyID string) (*cad.ExternalAcquisitionConnection, error) {

		var newFirst *wrapperspb.Int64Value 
		var newAfter *wrapperspb.Int64Value
		var newLast *wrapperspb.Int64Value
		var newBefore *wrapperspb.Int64Value
		if first != nil {
			newFirst = &wrapperspb.Int64Value {
				Value: *first,
			}
		}
		if after != nil {
			newAfter = &wrapperspb.Int64Value {
				Value: *after,
			}
		}
		if last != nil {
			newLast = &wrapperspb.Int64Value {
				Value: *last,
			}
		}
		if before != nil {
			newBefore = &wrapperspb.Int64Value {
				Value: *before,
			}
		}

	acquisitionInfo, err := r.Client.QueryExternalAcquisitionListByCompanyID(
		context.Background(),
		&cad.QueryExternalAcquisitionListByCompanyIDRequest{
			First:     newFirst,
			After:     newAfter,
			Last:      newLast,
			Before:    newBefore,
			CompanyId: companyID,
		},
	)

	if err != nil {
		log.Println(err)
		return nil, err
	}

	return acquisitionInfo, nil
}

func (r *queryResolver) QueryMergerInformationByCompanyID(ctx context.Context, companyID string) (*cad.MergerInfoResponse, error) {
	mergerInfo, err := r.Client.QueryMergerInformationByCompanyID(
		context.Background(),
		&cad.QueryMergerInformationByCompanyIDRequest{
			CompanyId: companyID,
		},
	)

	if err != nil {
		log.Println(err)
		return nil, err
	}

	return mergerInfo, nil
}

func (r *queryResolver) QueryFinancingOverviewByFinancingIndex(ctx context.Context, companyID string, fundingRoundType string, fundedAt string, raisedAmountUsd int64) (*cad.FinancingOverviewResponse, error) {

	financingOverview, err := r.Client.QueryFinancingOverviewByFinancingIndex(
		context.Background(),
		&cad.QueryFinancingOverviewByFinancingIndexRequest{
			CompanyId:        companyID,
			FundingRoundType: fundingRoundType,
			FundedAt:         fundedAt,
			RaisedAmountUsd:  raisedAmountUsd,
		},
	)

	if err != nil {
		return nil, err
	}

	return financingOverview, nil
}

func (r *queryResolver) QueryFinancingInvestorListByFinancingIndex(ctx context.Context, 
	first *int64,
	after *int64,
	last *int64,
	before *int64,
	companyID string, fundingRoundType string, fundedAt string, raisedAmountUsd int64) (*cad.InvestorListConnection, error) {

		var newFirst *wrapperspb.Int64Value 
		var newAfter *wrapperspb.Int64Value
		var newLast *wrapperspb.Int64Value
		var newBefore *wrapperspb.Int64Value
		if first != nil {
			newFirst = &wrapperspb.Int64Value {
				Value: *first,
			}
		}
		if after != nil {
			newAfter = &wrapperspb.Int64Value {
				Value: *after,
			}
		}
		if last != nil {
			newLast = &wrapperspb.Int64Value {
				Value: *last,
			}
		}
		if before != nil {
			newBefore = &wrapperspb.Int64Value {
				Value: *before,
			}
		}

	investorListConnection, err := r.Client.QueryFinancingInvestorListByFinancingIndex(
		context.Background(),
		&cad.QueryFinancingInvestorListByFinancingIndexRequest{
			First:     newFirst,
			After:     newAfter,
			Last:      newLast,
			Before:    newBefore,
			CompanyId:        companyID,
			FundingRoundType: fundingRoundType,
			FundedAt:         fundedAt,
			RaisedAmountUsd:  raisedAmountUsd,
		},
	)

	if err != nil {
		log.Println(err)
		return nil, err
	}

	return investorListConnection, nil
}

// Mutation returns MutationResolver implementation.
func (r *Resolver) Mutation() MutationResolver { return &mutationResolver{r} }

// Query returns QueryResolver implementation.
func (r *Resolver) Query() QueryResolver { return &queryResolver{r} }

type mutationResolver struct{ *Resolver }
type queryResolver struct{ *Resolver }
