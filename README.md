query queryCompanyOverview {
  QueryCompanyOverviewByCompanyID(companyID: "/company/169-st") {
    	city
      foundedAt
      statusCode 
      foundingRounds
      firstFunded {
        	companyID        
          fundingRoundType 
          fundedAt        
          raisedAmountUsd 
      }
      lastFunded {
        	companyID        
          fundingRoundType 
          fundedAt        
          raisedAmountUsd 
      }
      fundingTotalUSD 
      companyName
  }
}

query queryFinancingOverview {
  QueryFinancingOverviewByFinancingIndex(
    companyID: "/company/waywire"
		fundingRoundType: "series-a",
		fundedAt: "2012-06-30",
		raisedAmountUsd: 1750000,
  ) {
    companyInfo {
      companyID
      companyName
      categoryCode
    }
    financingInfo {
      companyID        
      fundingRoundType 
      fundedAt        
      raisedAmountUsd 
    }
  }
}

query queryCompanyList {
  QueryCompanyListByCompanyName (companyName: "Adocu.com") {
    companyInfo {
      companyID
      companyName
      categoryCode
    }
  }
}

query QueryFinancingInvestorList {
  QueryFinancingInvestorListByFinancingIndex(
			first: 5,
			after: 0,
			companyID: "/company/waywire",  
			fundingRoundType: "series-a",
			fundedAt: "2012-06-30",
			raisedAmountUsd: 1750000,
  ) {
    totalCount
    nodes {
      companyInfo {
        companyID
        companyName
        categoryCode
      }
    }
    pageInfo {
      endCursor
      startCursor
      hasNextPage
      hasPreviousPage
    }
  }
}

query QueryFinancingList {
  QueryFinancingListByCompanyID(
    first: 1,
    after: 0,
    companyID: "/company/121nexus",
  ) {
    totalCount
    nodes {
      investors {
        companyID
        companyName
        categoryCode
      }
      financingInfo{
        companyID        
        fundingRoundType 
        fundedAt        
        raisedAmountUsd 
      }
    }
    edges {
      node {
        investors {
          companyID
          companyName
          categoryCode
        }
        financingInfo{
          companyID        
          fundingRoundType 
          fundedAt        
          raisedAmountUsd 
      	}
      }
    }
    pageInfo {
      endCursor
      startCursor
      hasNextPage
      hasPreviousPage
    }
  }
}

query QueryInvestmentList {
  QueryInvestmentListByCompanyID (
    first: 2,
    after: 0,
    companyID: "/company/10xelerator",
  ) {
    totalCount
    nodes {
      investorInfo {
        companyID
        companyName
        categoryCode
      }
      financingInfo{
        companyID        
        fundingRoundType 
        fundedAt        
        raisedAmountUsd 
        companyName
      }
    }
    edges {
      node {
        investorInfo {
          companyID
          companyName
          categoryCode
        }
        financingInfo{
          companyID        
          fundingRoundType 
          fundedAt        
          raisedAmountUsd 
          companyName
      	}
      }
    }
    pageInfo {
      endCursor
      startCursor
      hasNextPage
      hasPreviousPage
    }
  }
}
query QueryExternalAcquisitionList {
  QueryExternalAcquisitionListByCompanyID (
    first: 5,
		after: 0,
		companyID: "/company/waywire",  
  ) {
    totalCount
    nodes {
      companyInfo {
        companyID
        companyName
        categoryCode
      }
      acquiredAt 
			priceAmount
    }
    edges {
      node {
        companyInfo {
        companyID
        companyName
        categoryCode
      }
      acquiredAt 
			priceAmount
      }
      cursor
    }
    pageInfo {
      endCursor
      startCursor
      hasNextPage
      hasPreviousPage
    }
  }
}

query QueryMergerInformation {
  QueryMergerInformationByCompanyID(companyID: "/company/adstack") {
    acquirerInfo {
			companyName
			companyID
			categoryCode
		},
		acquiredAt
		priceAmount
  }
}