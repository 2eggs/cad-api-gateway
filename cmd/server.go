package main

import (
	"log"
	"net/http"
	"os"

	"github.com/rs/cors"

	"github.com/99designs/gqlgen/graphql/handler"
	"github.com/99designs/gqlgen/graphql/playground"
	"github.com/go-chi/chi"

	"gitlab.com/mayunmeiyouming/cad-api-gateway/internal/resolvers"

	cad "gitlab.com/mayunmeiyouming/cad-micro/rpc/cad"
)

const defaultPort = "8080"

func main() {
	router := chi.NewRouter()

	router.Use(cors.New(cors.Options{
		AllowedOrigins:   []string{"http://localhost:3000", "http://192.168.144.129:3000"},
		AllowCredentials: true,
		Debug:            false,
	}).Handler)

	port := os.Getenv("PORT")
	if port == "" {
		port = defaultPort
	}
	client := cad.NewCadTwirpProtobufClient("http://localhost:1234", &http.Client{})

	srv := handler.NewDefaultServer(resolvers.NewExecutableSchema(resolvers.Config{Resolvers: &resolvers.Resolver{client}}))
	log.Printf("connect to http://localhost:%s/ for GraphQL playground", port)
	router.Handle("/", playground.Handler("GraphQL playground", "/query"))
	router.Handle("/query", srv)

	err := http.ListenAndServe(":8080", router)
	if err != nil {
		panic(err)
	}
	log.Fatal(http.ListenAndServe(":"+port, nil))
}
